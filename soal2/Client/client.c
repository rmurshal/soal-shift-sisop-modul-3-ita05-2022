#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080

int main(int argc, char const *argv[])
{
  struct sockaddr_in address;
  int sock = 0, valread;
  struct sockaddr_in serv_addr;
  char *hello = "Hello from client";
  char buffer[1024] = {0};

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("\n Socket creation error \n");
    return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
  {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("\nConnection Failed \n");
    return -1;
  }

  while (1)
  {
    char *input;
    printf("Pilihan auth (register / login): ");
    scanf("%s", input);
    send(sock, input, strlen(input), 0);

    if (strcmp(input, "register") == 0)
    {
      memset(buffer, 0, 1024);
      for (int i = 0; i < 2; i++)
      {
        if (i == 0)
        {
          printf("Create username: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
        else if (i == 1)
        {
          printf("Create password: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
      }
      valread = read(sock, buffer, 1024);

      if (strcmp(buffer, "YES") == 0)
      {
        printf("User berhasil ditambahkan \n");
      }
      else if (strcmp(buffer, "NO") == 0)
      {
        printf("User gagal ditambahkan \n");
      }
    }
    else if (strcmp(input, "login") == 0)
    {
      memset(buffer, 0, 1024);
      for (int i = 0; i < 2; i++)
      {
        if (i == 0)
        {
          printf("Input username: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
        else if (i == 1)
        {
          printf("Input password: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
      }
      valread = read(sock, buffer, 1024);

      if (strcmp(buffer, "YES") == 0)
      {
        // todo: add command 2c
        printf("Input perintah (add/see): ");
        scanf("%s", input);

        send(sock, input, strlen(input), 0);

        if (strcmp(input, "add") == 0)
        {
          for (int r = 0; r < 4; r++)
          {
            if (r == 0)
            {
              scanf("%s", input);
              send(sock, input, strlen(input), 0);
            }
            else if (r == 1)
            {
              scanf("%s", input);
              send(sock, input, strlen(input), 0);
            }
            else if (r == 2)
            {
              scanf("%s", input);
              send(sock, input, strlen(input), 0);
            }
            else if (r == 3)
            {
              scanf("%s", input);
              send(sock, input, strlen(input), 0);
            }
          }

          memset(buffer, 0, 1024);
          valread = read(sock, buffer, 1024);

          if (strcmp(buffer, "YES") == 0)
          {
            printf("Folder problem berhasil dibuat \n");
          }
          else if (strcmp(buffer, "NO") == 0)
          {
            printf("Problem sudah ada, folder gagal dibuat \n");
          }
        }
        else if (strcmp(input, "see") == 0)
        {
          memset(buffer, 0, 1024);
          valread = read(sock, buffer, 1024);
          printf("%s", buffer);
        }
      }
      else if (strcmp(buffer, "NO") == 0)
      {
        printf("Login gagal \n");
      }
    }

    printf("\n");
  }

  return 0;
}
