#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>

#define PORT 8080
pthread_t tid[1];

char *usersTxt = "users.txt";

struct UserData
{
  char id[100];
  char password[100];
};

struct ProblemData
{
  char judulProblem[100];
  char descriptionPath[100];
  char inputPath[100];
  char outputPath[100];
};

void *createTsv()
{
  FILE *f = fopen("problem.tsv", "a");
  fclose(f);
}

int isUserExist(struct UserData user)
{
  FILE *f = fopen(usersTxt, "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");

    if (strcmp(token, user.id) == 0)
    {
      return 1;
    }
  }

  fclose(f);
  return 0;
}

int isPasswordPass(struct UserData user)
{
  if (strlen(user.password) < 6)
  {
    return 0;
  }

  int i = 0;
  int statusPass = 0;
  while (user.password[i])
  {
    if (isdigit(user.password[i]))
    {
      statusPass = 1;
      break;
    }
    i++;
  }
  if (!statusPass)
  {
    return 0;
  }

  i = statusPass = 0;
  while (user.password[i])
  {
    if (isupper(user.password[i]))
    {
      statusPass = 1;
      break;
    }
    i++;
  }
  if (!statusPass)
  {
    return 0;
  }

  i = 0;
  while (user.password[i])
  {
    if (islower(user.password[i]))
    {
      return 1;
    }
    i++;
  }

  return 0;
}

int addUserToUsersTxt(struct UserData user)
{
  int userExist = 2;
  userExist = isUserExist(user);

  int passwordPass = 2;
  passwordPass = isPasswordPass(user);

  if (userExist == 1 || passwordPass == 0)
  {
    return 0;
  }
  else if (userExist == 0 && passwordPass == 1)
  {
    FILE *f = fopen(usersTxt, "a");
    if (f == NULL)
    {
      printf("Error opening users.txt file \n");
      exit(1);
    }

    fprintf(f, "%s:%s\n", user.id, user.password);
    fclose(f);

    return 1;
  }
}

int checkLogin(struct UserData user)
{
  FILE *f = fopen(usersTxt, "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");

    if (strcmp(token, user.id) == 0)
    {
      token = strtok(NULL, ":");
      strcat(user.password, "\n");

      if (strcmp(token, user.password) == 0)
      {
        return 1;
      }
    }
  }

  fclose(f);
  return 0;
}

int isProblemExist(char *problemName)
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir(".");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, "problem.tsv") != 0 && strcmp(ep->d_name, "server") != 0 && strcmp(ep->d_name, "server.c") != 0 && strcmp(ep->d_name, "users.txt") != 0)
      {
        if (strcmp(problemName, ep->d_name) == 0)
        {
          return 1;
        }
      }
    }
    return 0;
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void createProblemFolder(char *problemName)
{
  if (mkdir(problemName, 0777) == -1)
  {
    printf("Error when create folder %s \n", problemName);
    exit(0);
  }

  char desc[100], in[100], out[100];
  strcpy(desc, problemName);
  strcpy(in, problemName);
  strcpy(out, problemName);
  strcat(desc, "/description.txt");
  strcat(in, "/input.txt");
  strcat(out, "/output.txt");

  FILE *f1 = fopen(desc, "a");
  fclose(f1);
  FILE *f2 = fopen(in, "a");
  fclose(f2);
  FILE *f3 = fopen(out, "a");
  fclose(f3);
}

void editTsvFile(struct ProblemData problem, char *idCurrentUser)
{
  FILE *f = fopen("problem.tsv", "a");
  if (f == NULL)
  {
    printf("Error opening problem.txt file \n");
    exit(1);
  }

  fprintf(f, "%s:%s:%s:%s:%s\n", problem.judulProblem, idCurrentUser, problem.descriptionPath, problem.inputPath, problem.outputPath);
  fclose(f);
}

void showListProblem()
{
  FILE *f = fopen("problem.tsv", "r");
  if (f == NULL)
  {
    printf("Error opening problem.tsv file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");
    printf("%s by ", token);
    token = strtok(NULL, ":");
    printf("%s\n", token);
  }
  printf("\n");

  fclose(f);
}

int main(int argc, char const *argv[])
{
  int server_fd, new_socket, valread;
  struct sockaddr_in address;
  int opt = 1;
  int addrlen = sizeof(address);
  char buffer[1024] = {0};
  char *hello = "Hello from server";

  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }

  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
  {
    perror("setsockopt");
    exit(EXIT_FAILURE);
  }

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
  {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  if (listen(server_fd, 1000) < 0)
  {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
  {
    perror("accept");
    exit(EXIT_FAILURE);
  }

  int x = 0;
  while (x < 1)
  {
    int errTsv = pthread_create(&(tid[0]), NULL, &createTsv, NULL);
    x++;
  }
  pthread_join(tid[0], NULL);

  int index = 0;
  struct UserData user;
  struct ProblemData problem;

  while (1)
  {
    memset(buffer, 0, 1024);
    valread = read(new_socket, buffer, 1024);

    if (strcmp(buffer, "register") == 0)
    {
      for (int i = 0; i < 2; i++)
      {
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);

        if (i == 0)
        {
          strcpy(user.id, buffer);
        }
        else if (i == 1)
        {
          strcpy(user.password, buffer);
        }
      }

      int isSend = addUserToUsersTxt(user);
      if (isSend)
      {
        char *msg = "YES";
        send(new_socket, msg, strlen(msg), 0);
      }
      else
      {
        char *msg = "NO";
        send(new_socket, msg, strlen(msg), 0);
      }
    }
    else if (strcmp(buffer, "login") == 0)
    {
      for (int i = 0; i < 2; i++)
      {
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);

        if (i == 0)
        {
          strcpy(user.id, buffer);
        }
        else if (i == 1)
        {
          strcpy(user.password, buffer);
        }
      }

      int isLogin = checkLogin(user);
      if (isLogin)
      {
        char *msg = "YES";
        send(new_socket, msg, strlen(msg), 0);

        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);

        if (strcmp(buffer, "add") == 0)
        {
          for (int r = 0; r < 4; r++)
          {
            memset(buffer, 0, 1024);

            if (r == 0)
            {
              printf("Judul problem: \n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.judulProblem, buffer);
            }
            else if (r == 1)
            {
              printf("Filepath description.txt: \n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.descriptionPath, buffer);
            }
            else if (r == 2)
            {
              printf("Filepath input.txt: \n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.inputPath, buffer);
            }
            else if (r == 3)
            {
              printf("Filepath output.txt: \n\n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.outputPath, buffer);
            }
          }

          int problemExist = isProblemExist(problem.judulProblem);
          if (problemExist)
          {
            char *msg = "NO";
            send(new_socket, msg, strlen(msg), 0);
          }
          else
          {
            createProblemFolder(problem.judulProblem);
            editTsvFile(problem, user.id);
            char *msg = "YES";
            send(new_socket, msg, strlen(msg), 0);
          }
        }
        else if (strcmp(buffer, "see") == 0)
        {
          char *msg = "Silahkan lihat di sisi server untuk list problem \n";
          send(new_socket, msg, strlen(msg), 0);
          showListProblem();
        }
      }
      else
      {
        char *msg = "NO";
        send(new_socket, msg, strlen(msg), 0);
      }
    }
  }

  return 0;
}
