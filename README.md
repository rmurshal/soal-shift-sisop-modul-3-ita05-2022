[](url)# Soal Shift Sistem Operasi Modul-3 ITA05
Anggota kelompok:
- Sarah Hanifah Pontoh - 5027201006
- Rama Muhammad Murshal - 5027201041
- Muhammad Rifqi Fernanda - 5027201050

# Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

## Soal 1
*Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.*

### Soal 1.a
*Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.*

```c
pthread_t tid_unzip[2];
void *thread_unzip(void *arg) {
    pthread_t id=pthread_self();

    if(pthread_equal(id,tid_unzip[0])) {
        unzip_file("music.zip");
    } else if(pthread_equal(id,tid_unzip[1])) {
        unzip_file("quote.zip");
    }
    return NULL;
}
```
Setelah music.zip dan quote.zip didownload pada directory, dijalankan thread `tid_unzip` untuk mengunzip keduanya. Kedua thread ini menggunakan fungsi `unzip_file` untuk mengekstrak setiap file dalam kedua folder zip.

```c
void unzip_file(char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv[] = {"unzip","-q", name_file, NULL};
        execv("/bin/unzip", argv);
        exit(EXIT_SUCCESS);

    } else {
        wait(NULL);
    }
}
```

Setelah itu, thread dipanggil di main() dan dilakukan thread join untuk keduanya.

```c
while(i < 2) {
		err1 = pthread_create(&(tid_unzip[i]),NULL,&thread_unzip,NULL); 
		if(err1!=0) {
			printf("\n can't create thread : [%s]",strerror(err1));
		} else {
			printf("\n create thread success\n");
		}

		i++;
	}

    pthread_join(tid_unzip[0],NULL);
	pthread_join(tid_unzip[1],NULL);
```

### Soal 1.b
*Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.*

Digunakan fungsi `base64_decode` untuk mendecode isi dari txt file ke ASCII characters sehingga dapat dibaca.

```c
char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';
    return plain;
}
```
`base64_decode` tersebut akan dipanggil saat menjalankan thread untuk mengisi file `music.txt` dan `quote.txt`. Sebelumnya, kita harus membuat kedua file txt tersebut terlebih dahulu.

```c
void create_file(char *name) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv[] = {"touch", name, NULL};
        execv("/bin/touch", argv);
        
    } else {
        wait(NULL);
    }
}
```
Selanjutnya, pada thread pertama akan membuat file `music.txt` dan mengisinya dengan hasil decode. Lalu pada thread kedua akan dialkukan hal yang sama namun untuk `quote.txt`.

```c
pthread_t tid_file[2];
void * thread_create_file(void *arg) {
    pthread_t id=pthread_self();
    pid_t child_id;
    child_id = fork();

    if(pthread_equal(id,tid_file[0])) {
        create_file("music.txt");
        FILE* ptr, *fp;
        char word[100];
        char files[100][100];
        strcpy(files[0], "m1.txt");
        strcpy(files[1], "m2.txt");
        strcpy(files[2], "m3.txt");
        strcpy(files[3], "m4.txt");
        strcpy(files[4], "m5.txt");
        strcpy(files[5], "m6.txt");
        strcpy(files[6], "m7.txt");
        strcpy(files[7], "m8.txt");
        strcpy(files[8], "m9.txt");

        for(int i = 0; i<9; ++i) {
            FILE* ptr, *fp;
            char str[50];
            ptr = fopen(files[i], "r");
            fp = fopen("music.txt", "a");
            
            if (NULL == ptr) {
                    printf("file can't be opened \n");
            }
                
                while (fgets(str, 100, ptr) != NULL) {
                    fprintf(fp, "%s\n", base64_decode(str));
                    fclose(fp);
                }    
                fclose(ptr);    
            }
```

Untuk mengisi `quote.txt`:
```c
} else if(pthread_equal(id, tid_file[1])) {
        create_file("quote.txt");
        FILE* ptr, *fp;
        char str[50];
        char word[100];
        char files[100][100];
        strcpy(files[0], "q1.txt");
        strcpy(files[1], "q2.txt");
        strcpy(files[2], "q3.txt");
        strcpy(files[3], "q4.txt");
        strcpy(files[4], "q5.txt");
        strcpy(files[5], "q6.txt");
        strcpy(files[6], "q7.txt");
        strcpy(files[7], "q8.txt");
        strcpy(files[8], "q9.txt");

        for(int i = 0; i<9; ++i) {
            FILE* ptr, *fp;
            char str[50];
            ptr = fopen(files[i], "r");
            fp = fopen("quote.txt", "a");
            
            if (NULL == ptr) {
                    printf("file can't be opened \n");
                }
                
                while (fgets(str, 100, ptr) != NULL) {
                    fprintf(fp, "%s\n", base64_decode(str));
                    fclose(fp);
                }    

                fclose(ptr);    
            }
```
Kedua thread `tid_file` ini lalu dipanggil di main() seperti biasa dan dilakukan join.

```c
while(j < 2) {
		err2 = pthread_create(&(tid_file[j]),NULL,&thread_create_file,NULL); 
		if(err2!=0) {
			printf("\n can't create thread : [%s]",strerror(err2));
            
		} else {
			printf("\n create thread success\n");
		}

		j++;
	}


    pthread_join(tid_file[0],NULL);
	pthread_join(tid_file[1],NULL);

```

### Soal 1.c
*Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.*

Buat terlebih dahulu directory `hasil` di main().
```c
if(child_id == 0) {
        char *argv_hasil[] = {"mkdir", "-p", "hasil", NULL};
        execv("/bin/mkdir", argv_hasil);
    } else {
        wait(NULL);
    }
```

Setelah itu, untuk memindahkan kedua file tadi kita tinggal menambahkan process di setiap thread `tid_file` music dan quote.
```c
        fclose(ptr);    
        }
    

    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_music[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/bin/mv", argv_music);

    } else {
        wait(NULL);
    }
} else if(pthread_equal(id, tid_file[1])) {
  .
  .
  .
```
```c
  .
  .
  .
  fclose(ptr);    
      }

  if(child_id < 0) exit(EXIT_FAILURE);

  if(child_id == 0) {
      char *argv_quote[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
      execv("/bin/mv", argv_quote);

  } else {
      wait(NULL);
  }
```

### Soal 1.d
*Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)*

Membuat process baru di main() untuk men-zip dan set password untuk folder `hasil`.
```c
char zip_password[1000];
    strcpy(zip_password, "minihomenestsarah");
    strcat(zip_password, getlogin());

    pid_t child_id_1;
    child_id_1 = fork();

    if(child_id_1 < 0) exit(EXIT_FAILURE);

    if(child_id_1 == 0) {
        char *argv_password_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "hasil", NULL};
        execv("/bin/zip", argv_password_zip);
    } else {
        wait(NULL);
    }
```

### Soal 1.e
*Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.*

Dibuat 2 thread baru lagi, yaitu `tid_unzip_no_file`. Yang pertama untuk mengunzip `hasil.zip` dan yang kedua untuk membuat `no.txt` sekaligus menuliskan `No`.

```c
pthread_t tid_unzip_no_file[2];
void * unzip_no_file(void *arg) {
    pthread_t id=pthread_self();
    pid_t child_id;
    child_id = fork();

    if(pthread_equal(id,tid_unzip_no_file[0])) {
        char zip_password[1000];
        strcpy(zip_password, "minihomenestsarah");
        strcat(zip_password, getlogin());
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            char *argv_unzip[] = {"unzip", "-qo", "-P", zip_password, "hasil.zip", NULL};
            execv("/bin/unzip", argv_unzip);
        } else {
            wait(NULL);
        }
    } else if(pthread_equal(id,tid_unzip_no_file[1])) {
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            create_file("no.txt");
            FILE* fp = fopen("no.txt", "a");
            fprintf(fp, "No");
            fclose(fp);
        }
    }
}
```

Lalu pada fungsi main() dibuat prpocess untuk men-set password untuk `hasil.zip`
```c
if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "./hasil", "./no.txt", NULL};
        execv("/bin/zip", argv_zip);
    }
```
### Screenshot

Setelah di run, pada direktori muncul file dan folder sebgaai berikut. **music.zip** dan **quote.zip** sebelumnya sudah didownload sebelum program di run.

![command](./images/run.png)


### Kendala
1. Masih ada kesalahan, kalimat di `music.txt` dan `quote.txt` double, sehingga untuk masing-masing terdapat 18 kalimat.
![command](./images/error1.png)

2. Password yang dimasukkan masih salah, padahal sudah sesuai dengan sourcecode.
![command](./images/error2.png)


## Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

### Ketentuan Soal 2
- Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming.
- Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
- Untuk error handling jika tidak diminta di soal tidak perlu diberi.

Struktur Direktori Client Server:

![Gambar Struktur Direktori Client Server](./images/2struktur.png)

### Soal 2.a
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

Format users.txt:

![Format users.txt](./images/2formatUsers.png)

### Pembahasan Soal 2.a - Server
Pertama-tama, kita menyiapkan header-header yang akan digunakan dalam mengerjakan soal, seperti berikut:

```c
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <dirent.h>
```

Lalu, menyiapkan konstanta nilai port yang akan digunakan, variable pthread_t yang bakal digunakan untuk membuat thread, serta string global yang menyimpan karakter "users.txt" sebagai berikut:


```c
#define PORT 8080
pthread_t tid[1];
char *usersTxt = "users.txt";
```

Lalu, menyiapkan sebuah struct `UserData` yang bakal digunakan untuk operasi login / registrasi dari user, seperti berikut:

```c
struct UserData
{
  char id[100];
  char password[100];
};
```

Kemudian, membuat sebuah fungsi `isUserExist()` yang akan mengecek apakah suatu username user sudah ada di `users.txt` atau belum. Fungsi ini dibutuhkan saat registrasi. Fungsi tersebut sesuai berikut:

```c
int isUserExist(struct UserData user)
{
  FILE *f = fopen(usersTxt, "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");

    if (strcmp(token, user.id) == 0)
    {
      return 1;
    }
  }

  fclose(f);
  return 0;
}
```

Fungsi ini membutuhkan argumen struct dari `UserData` untuk menyimpan username dari user yang akan dicek. Isi dari fungsi ini adalah membuka isi dari `users.txt`, lalu membaca setiap kata yang ada perbarisnya, lalu melakukan `strtok()` dengan pemisah `:` untuk mendapatkan kata pertama dari tiap barisnya tersebut (kata pertama merupakan username user). Apabila kata pertama tersebut sesuai dengan `user.id`, maka return 1. Apabila tidak, return 1.

Kemudian, membuat sebuah fungsi `isPasswordPass()` untuk mengecek apakah password yang diinputkan user saat registrasi sudah sesuai dengan kriteria atau belum. Isi fungsi ini sebagai berikut:

```c
int isPasswordPass(struct UserData user)
{
  if (strlen(user.password) < 6)
  {
    return 0;
  }

  int i = 0;
  int statusPass = 0;
  while (user.password[i])
  {
    if (isdigit(user.password[i]))
    {
      statusPass = 1;
      break;
    }
    i++;
  }
  if (!statusPass)
  {
    return 0;
  }

  i = statusPass = 0;
  while (user.password[i])
  {
    if (isupper(user.password[i]))
    {
      statusPass = 1;
      break;
    }
    i++;
  }
  if (!statusPass)
  {
    return 0;
  }

  i = 0;
  while (user.password[i])
  {
    if (islower(user.password[i]))
    {
      return 1;
    }
    i++;
  }

  return 0;
}
```

Fungsi ini membutuhkan argumen struct `UserData` yang menyimpan paassword dari user yang akan dicek. Isi dari fungsi ini, pertama, mengecek apakah password yang diinputkan user sudah lebih dari 6 karakter, apabila belum, return 0. Kedua, mengecek apakah passwordnya telah mengandung digit angka (menggunakan fungsi `isdigit()`), apabila iya maka lanjut ke pengecekan berikutnya, apabila belum maka return 0. Ketiga, mengecek apakah password telah mengandung karakter huruf besar (menggunakan fungsi `isupper()`), apabila iya maka lanjut ke pengecekan berikutnya, apabila belum maka return 0. Keempat, mengecek apakah password telah mengandung karakter huruf kecil (menggunakan fungsi fungsi `islower()`), apabila iya maka return 1 yang artinya password telah memenuhi kriteria, apabila tidak maka return 0.

Kemudian, membuat fungsi `addUserToUsersTxt()` yang akan menambahkan user ke `users.txt`. Fungsi ini merupakan core fungsi untuk registrasi. Fungsi ini sesuai dengan:

```c
int addUserToUsersTxt(struct UserData user)
{
  int userExist = 2;
  userExist = isUserExist(user);

  int passwordPass = 2;
  passwordPass = isPasswordPass(user);

  if (userExist == 1 || passwordPass == 0)
  {
    return 0;
  }
  else if (userExist == 0 && passwordPass == 1)
  {
    FILE *f = fopen(usersTxt, "a");
    if (f == NULL)
    {
      printf("Error opening users.txt file \n");
      exit(1);
    }

    fprintf(f, "%s:%s\n", user.id, user.password);
    fclose(f);

    return 1;
  }
}
```

Fungsi ini membutuhkan argumen struct `UserData` yang akan menyimpan data username dan password user yang mencoba registrasi. Isi dari fungsi ini yaitu akan memanggil fungsi `isUserExist()` dan `isPasswordPass()` untuk melakukan fungsi registrasi. Apabila `isUserExist()` me-return 1 atau `isPasswordPass()` me-return nilai 0, maka fungsi ini akan me-return 0 yang berarti bahwa registrasi gagal dilakukan. Apabila fungsi `isUserExist()` me-return nilai 0 dan `isPasswordPass()` me-return nilai 1, maka fungsi akan me-return nilai 1 yang berarti bahwa registrasi berhasil dilakukan. Tetapi, sebelum melakukan return 1, fungsi pertama-tama akan melakukan wirte data username dan password dari user yang berhasil registrasi kedalam file `users.txt` dengan format `<username>:<password>`.

Kemudian, membuat fungsi `checkLogin()` untuk mengecek apakah user berhasil melakukan autentikasi atau tidak. Fungsi ini sesuai berikut:

```c
int checkLogin(struct UserData user)
{
  FILE *f = fopen(usersTxt, "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");

    if (strcmp(token, user.id) == 0)
    {
      token = strtok(NULL, ":");
      strcat(user.password, "\n");

      if (strcmp(token, user.password) == 0)
      {
        return 1;
      }
    }
  }

  fclose(f);
  return 0;
}
```

Fungsi ini membutuhkan argumen struct `UserData` yang akan menyimpan data username dan password user yang mencoba login. Isi dari fungsi ini, pertama membuka file `users.txt` dan membaca setiap kata yang ada di setiap barisnya. Kedua, melakukan `strtok()` dengan pemisah `:` untuk mendapatkan karakter pertama (username) di setiap barisnya. Ketiga, mengecek apakah kata pertama tersebut sesuai dengan username yang di-pass kepada fungsi, apabila iya lanjut ke pengecekan password. Keempat, mengecek apakah kata keduanya tersebut sesuai dengan password yang di-pass kepada fungsi, apabila iya maka return 1 yang berarti login berhasil dilakukan. Apabila hingga akhir perulangan belum melakukan return 1, maka langsung return 0 yang berarti login gagal dilakukan.

Selanjutnya, pada fungsi `main()`, pertama-tama kita menyiapkan variabel-variabel yang berguna untuk membuat socket serta melakukan operasi-operasi di dalamnya. Variable-variabel tersebut sesuai dengan:

```c
int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char buffer[1024] = {0};
char *hello = "Hello from server";
```

Kemudian, melakukan setup socket dengan melakukan pemanggilan-pemanggilan fungsi socket yang dibutuhkan. Hal ini sesuai dengan:

```c
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }

  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
  {
    perror("setsockopt");
    exit(EXIT_FAILURE);
  }

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
  {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  if (listen(server_fd, 1000) < 0)
  {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
  {
    perror("accept");
    exit(EXIT_FAILURE);
  }
```

Lalu, membuat variable struct dari struct yang telah didefinisikan diatas sesuai dengan:

```c
struct UserData user;
```

Kemudian, melakukan while infinite untuk melakukan fungsi registrasi dan login untuk client. While loop infinite ini sesuai dengan:

```c
  while (1)
  {
    memset(buffer, 0, 1024);
    valread = read(new_socket, buffer, 1024);

    if (strcmp(buffer, "register") == 0)
    {
      for (int i = 0; i < 2; i++)
      {
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);

        if (i == 0)
        {
          strcpy(user.id, buffer);
        }
        else if (i == 1)
        {
          strcpy(user.password, buffer);
        }
      }

      int isSend = addUserToUsersTxt(user);
      if (isSend)
      {
        char *msg = "YES";
        send(new_socket, msg, strlen(msg), 0);
      }
      else
      {
        char *msg = "NO";
        send(new_socket, msg, strlen(msg), 0);
      }
    }
    else if (strcmp(buffer, "login") == 0)
    {
      for (int i = 0; i < 2; i++)
      {
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);

        if (i == 0)
        {
          strcpy(user.id, buffer);
        }
        else if (i == 1)
        {
          strcpy(user.password, buffer);
        }
      }

      int isLogin = checkLogin(user);
      if (isLogin)
      {
        char *msg = "YES";
        send(new_socket, msg, strlen(msg), 0);
      }
      else
      {
        char *msg = "NO";
        send(new_socket, msg, strlen(msg), 0);
      }
    }
  }
```

Isi dari while infinite tersebut, pertama-tama membaca pesan yang dikirimkan oleh client menggunakan fungsi `read()`, isinya bakal disimpan pada variabel `valread`. Isi dari pesan pertama tersebut antara karakter register atau login, sesuai dengan apa yang ingin dilakukan oleh user.

Apabila isi pesan pertama tersebut merupakan karakter `register`, maka server akan melakukan for loop sebanyak dua iterasi, yang masing-masing iterasinya akan menangkap pesan yang dikirimkan oleh client. Data pesan iterasi pertama akan disimpan pada member id dari struct, sedangkan data pesan iterasi kedua akan disimpan pada member password dari struct. Setelah itu, akan memanggil fungsi `addUserToUserstxt()` yang nilai kembaliannya akan disimpan ada pada variabel `isSend`. Apabila nilainya 1 maka server akan mengirimkan pesan "YES" kepada user, sedangkan apabila 0 maka server akan mengirimkan pesan "NO" kepada user.

Apabila isi pesan pertama tersebut merupakan karakter `login`, maka server akan melakukan for loop sebanyak dua iterasi, yang masing-masing iterasinya akan menangkap pesan yang dikirimkan oleh client. Data pesan iterasi pertama akan disimpan pada member id dari struct, sedangkan data pesan iterasi kedua akan disimpan pada member password dari struct. Setelah itu, akan memanggil fungsi `checkLogin()` yang nilai returnnya akan disimpan pada variable `isLogin`. Apabila nilainya 1 maka server akan mengirimkan pesan "YES" kepada user, sedangkan apabila 0 maka server akan mengirimkan pesan "NO" kepada user.

### Soal 2.b
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

### Pembahasan Soal 2.b - Server
Pertama-tama membuat fungsi `createTsv()` yang akan dipanggil oleh `pthread_create()` untuk membuat file tsv. Isi dari fungsi ini sesuai:

```c
void *createTsv()
{
  FILE *f = fopen("problem.tsv", "a");
  fclose(f);
}
```

Lalu, pada fungsi `main()`, kita sisa memanggil fungsi tersebut pada `pthread_create()` sesuai:

```c
  int x = 0;
  while (x < 1)
  {
    int errTsv = pthread_create(&(tid[0]), NULL, &createTsv, NULL);
    x++;
  }
  pthread_join(tid[0], NULL);
```

### Soal 2.c
Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

Contoh:

![Contoh command implementasi 2c](./images/2ssContohCS.png)

Seluruh file akan disimpan oleh server ke dalam folder dengan nama judul-problem yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.

### Pembahasan Soal 2.c - server
Pertama-tama, membuat fungsi `isProblemExist()` yang akan mengecek apakah nama problem yang diinputkan user ada atau tidak. Fungsi tersebut sesuai:

```c
int isProblemExist(char *problemName)
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir(".");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, "problem.tsv") != 0 && strcmp(ep->d_name, "server") != 0 && strcmp(ep->d_name, "server.c") != 0 && strcmp(ep->d_name, "users.txt") != 0)
      {
        if (strcmp(problemName, ep->d_name) == 0)
        {
          return 1;
        }
      }
    }
    return 0;
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Fungsi akan membuka folder saat ini (server) dan membacanya. Akan melakukan return 1 apabila folder telah ada, dan return 0 jika belum ada.

Kemudian, membuat fungsi `createProblemFolder()` yang akan berguna folder nama problem yang diinputkan oleh user, serta membuat file `description.txt`, `input.txt`, dan `output.txt` di dalam folder tersebut. Fungsi ini sesuai dengan:

```c
void createProblemFolder(char *problemName)
{
  if (mkdir(problemName, 0777) == -1)
  {
    printf("Error when create folder %s \n", problemName);
    exit(0);
  }

  char desc[100], in[100], out[100];
  strcpy(desc, problemName);
  strcpy(in, problemName);
  strcpy(out, problemName);
  strcat(desc, "/description.txt");
  strcat(in, "/input.txt");
  strcat(out, "/output.txt");

  FILE *f1 = fopen(desc, "a");
  fclose(f1);
  FILE *f2 = fopen(in, "a");
  fclose(f2);
  FILE *f3 = fopen(out, "a");
  fclose(f3);
}
```

Fungsi ini membutuhkan argumen `problemName` yang akan menyimpan nama problem. Isi dari fungsi ini adalah pertama-tama membuat folder di dalam server sesuai nama problemnya dengan memanggil fungsi `mkdir()`. Kedua, membuat file `description.txt`, `input.txt`, dan `output.txt` lalu kemudian menyimpannya ke dalam folder yang dibuat tersebut.

Lalu, membuat fungsi `editTsvFile()` yang akan melakukan edit dari file `problem.tsv` sesuai problem yang diinputkan oleh user. Fungsi ini sesuai dengan:

```c
void editTsvFile(struct ProblemData problem, char *idCurrentUser)
{
  FILE *f = fopen("problem.tsv", "a");
  if (f == NULL)
  {
    printf("Error opening problem.txt file \n");
    exit(1);
  }

  fprintf(f, "%s:%s:%s:%s:%s\n", problem.judulProblem, idCurrentUser, problem.descriptionPath, problem.inputPath, problem.outputPath);
  fclose(f);
}
```

Pada fungsi tersebut, kita menyimpan data problem beserta authornya dengan format judulProblem:author:description:input:output pada file problem.tsv. 

Kemudian pada fungsi `main()`, kita sisa menambahkan kode yang sesuai apabila user berhasil login. Kode yang ditambahkan sesuai dengan:

```c
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);

        if (strcmp(buffer, "add") == 0)
        {
          for (int r = 0; r < 4; r++)
          {
            memset(buffer, 0, 1024);

            if (r == 0)
            {
              printf("Judul problem: \n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.judulProblem, buffer);
            }
            else if (r == 1)
            {
              printf("Filepath description.txt: \n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.descriptionPath, buffer);
            }
            else if (r == 2)
            {
              printf("Filepath input.txt: \n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.inputPath, buffer);
            }
            else if (r == 3)
            {
              printf("Filepath output.txt: \n\n");
              valread = read(new_socket, buffer, 1024);
              strcpy(problem.outputPath, buffer);
            }
          }

          int problemExist = isProblemExist(problem.judulProblem);
          if (problemExist)
          {
            char *msg = "NO";
            send(new_socket, msg, strlen(msg), 0);
          }
          else
          {
            createProblemFolder(problem.judulProblem);
            editTsvFile(problem, user.id);
            char *msg = "YES";
            send(new_socket, msg, strlen(msg), 0);
          }
        }
```

### Soal 2.d
Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

![Gambar command see](./images/2see.png)

### Pembahasan Soal 2.d - server
Kita langsung membuat fungsi yang akan menampilkan semua problem yang ada pada file `problem.tsv`. Isi dari fungsi tersebut adalah membuka isi file `problem.tsv` terlebih dahulu, lalu melakukan `strtok()` dengan pemisah `:` dan mendapatkan kata pertama dan keduanya. Lalu sisa diprint berdasarkan format yang ditentukan. Fungsi tersebut sesuai dengan:

```c
void showListProblem()
{
  FILE *f = fopen("problem.tsv", "r");
  if (f == NULL)
  {
    printf("Error opening problem.tsv file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");
    printf("%s by ", token);
    token = strtok(NULL, ":");
    printf("%s\n", token);
  }
  printf("\n");

  fclose(f);
}
```

Kemudian pada fungsi `main()`, kita sisa menambahkan kode yang sesuai yakni apabila user menginputkan perintah `see`:

```c
        else if (strcmp(buffer, "see") == 0)
        {
          char *msg = "Silahkan lihat di sisi server untuk list problem \n";
          send(new_socket, msg, strlen(msg), 0);
          showListProblem();
        }
```

### Screenshot Contoh Implementasi

![Implementasi no 2](./images/2ssClient.png)

![Implementasi no 2](./images/2ssServer.png)

![Implementasi no 2](./images/2ssFolder.png)

## Soal 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
### Ketentuan Soal 3
Kategori folder tidak dibuat secara manual, harus melalui program C
Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
Dilarang juga menggunakan fork, exec dan system(), kecuali untuk bagian zip pada soal d

- Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming.
- Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
- Untuk error handling jika tidak diminta di soal tidak perlu diberi.


### Pembahasan Soal 3.a Eksrak zip 
### A.Eksrak zip 
Pertama kita harus mengestrak file hartakarun.zip. Lalu menggunakan fungsi fread untuk variabel chars_read dan akan membuat proses ekstraksi file  dalam arsip ke terminal.  Dan juga menggunakan fungsi extract().

```c
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```
### B.Mengkategorikan File
```c
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/rifqi/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Maaf kawan, lupakan saja :)\n");
    }else {
        printf("Kamu bisa lanjut, tapi jangan berharap lebih :)\n");
    }
}
```

Pada kategori ini program akan membaca hasil ekstrak. zip dan langsung akan mengecek satu-satu.

```c
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);
            
``` 
### C.Mengkategorikan File dengan menggunakan Thread
```c
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}
```
Pada fungsi ini yaitu nama file ke dalam variabel file_name dengan bantuan variabel buffer yang sebelumnya akan diisi nama file dengan bantuan fungsi strtok untuk mencari delimiter "/".
```c
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }
```  
Lalu untuk eksintensi file dengan bantuan variabel buffer.Terdapat pengecualian pada pengkategorian file ini yaitu apabila variabel buffer berisi NULL yang artinya eksintensi file akan dikategorikan hidden 

```c
    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }
```
### Kendala
belum menyelesaikan client server





